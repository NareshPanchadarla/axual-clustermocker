package io.springboot.mockserver;

import io.springboot.mockserver.controller.EmbeddedTomcatConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication(scanBasePackages = { "io.springboot.mockserver.controller","io.springboot.mockserver.service" })
@Import({EmbeddedTomcatConfiguration.class})
public class SpringbootMockserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMockserverApplication.class, args);
    }
}
