package io.springboot.mockserver.io.springboot.mockserver.data;

public enum StatusCode {

    TRUE("True"),
    FALSE("False"),
    UNRESPONSIVE("Unresponsive");

    private final String displayName;

    private StatusCode(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }


}
