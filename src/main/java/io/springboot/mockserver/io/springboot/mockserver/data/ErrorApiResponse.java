package io.springboot.mockserver.io.springboot.mockserver.data;


public class ErrorApiResponse {

    private Long status;
    private Boolean active;

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean  active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "ErrorApiResponse{" +
                "status=" + status +
                ", active='" + active + '\'' +
                '}';
    }
}
