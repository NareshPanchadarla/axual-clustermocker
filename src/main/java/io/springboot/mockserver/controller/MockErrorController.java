package io.springboot.mockserver.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;

@RestController
public class MockErrorController implements ErrorController {

    private static Logger logger = LogManager.getLogger(MockErrorController.class);

    @RequestMapping(value = "/error", produces = MediaType.APPLICATION_JSON_VALUE)
    public LinkedHashMap<String, String> handleError(HttpServletRequest request, HttpServletResponse response) {
        if(logger.isDebugEnabled()){
            logger.debug("MockErrorController:error("+request+","+response+")");
        }
        LinkedHashMap<String, String>  errorResponse = new LinkedHashMap<>();
        errorResponse.put("status", String.valueOf(response.getStatus()));
        errorResponse.put("message", "Resource not found");
        return errorResponse;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
