package io.springboot.mockserver.controller;

import io.springboot.mockserver.io.springboot.mockserver.data.ErrorApiResponse;
import io.springboot.mockserver.service.MockServerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/admin")
public class MockServerController {

    @Autowired
    private MockServerService mockServerService;

    private static Logger logger = LogManager.getLogger(MockServerController.class);

    @RequestMapping(value = "/active", produces = MediaType.APPLICATION_JSON_VALUE)
    public ErrorApiResponse active(HttpServletRequest request, HttpServletResponse response) {
        if(logger.isDebugEnabled()){
            logger.debug("MockServerController:active("+request+","+response+")");
        }

        return mockServerService.getStatusCode(request,response, null, null,  null);
    }

    @RequestMapping(value = "/activate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ErrorApiResponse activate(HttpServletRequest request, HttpServletResponse response) {
        if(logger.isDebugEnabled()){
            logger.debug("MockServerController:activate("+request+","+response+")");
        }
        return mockServerService.getStatusCode(request, response, "activate", null,  null);
    }

    @RequestMapping(value = "/deactivate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ErrorApiResponse deactivate(HttpServletRequest request, HttpServletResponse response ) {
        if(logger.isDebugEnabled()){
            logger.debug("MockServerController:deactivate("+request+","+response+")");
        }
        return mockServerService.getStatusCode(request, response, null, "deactivate",  null);
    }

    @RequestMapping(value = "/unresponsive", produces = MediaType.APPLICATION_JSON_VALUE)
    public ErrorApiResponse unresponsive(HttpServletRequest request, HttpServletResponse response ) {
        if(logger.isDebugEnabled()){
            logger.debug("MockServerController:unresponsive("+request+","+response+")");
        }
        return mockServerService.getStatusCode(request, response, null, null, "unresponsive");
    }

}
