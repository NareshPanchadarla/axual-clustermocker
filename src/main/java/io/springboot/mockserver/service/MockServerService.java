package io.springboot.mockserver.service;

import io.springboot.mockserver.controller.MockServerController;
import io.springboot.mockserver.io.springboot.mockserver.data.ErrorApiResponse;
import io.springboot.mockserver.io.springboot.mockserver.data.StatusCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MockServerService {

    @Value("${server.port}")
    private String serverPort;

    @Value("${server.additionalPorts}")
    private List<String> additionalPorts;

    private Map<String, String> networkedClusters = new HashMap<>();

    private static Logger logger = LogManager.getLogger(MockServerController.class);

    ErrorApiResponse errorApiResponse = new ErrorApiResponse();

    public ErrorApiResponse getStatusCode(HttpServletRequest request, HttpServletResponse response, String activate, String deactivate, String unresponsive) {
        if(logger.isDebugEnabled()){
            logger.debug("MockServerService:getStatusCode("+request+", "+response+","+activate+", "+deactivate+", "+unresponsive+")");
        }
        if (serverPort.equals(String.valueOf(request.getLocalPort()))) {
            errorApiResponse = setStatusCode(serverPort, response, activate, deactivate, unresponsive);
        } else {
            for (String port : additionalPorts) {
                if (port.equals(String.valueOf(request.getLocalPort()))) {
                    errorApiResponse = setStatusCode(port, response, activate, deactivate, unresponsive);
                    break;
                }
            }
        }
        return errorApiResponse;
    }

    public ErrorApiResponse setStatusCode(String port, HttpServletResponse response, String activate, String deactivate, String unresponsive) {
        if(logger.isDebugEnabled()){
            logger.debug("MockServerService:setStatusCode("+port+", "+response+","+activate+", "+deactivate+", "+unresponsive+")");
        }

        String portStatus = networkedClusters.get(port);

        if ((portStatus == null && (activate != null && activate.equalsIgnoreCase("activate"))) || (portStatus != null && (activate != null && activate.equalsIgnoreCase("activate")))) {
            networkedClusters.put(port, StatusCode.TRUE.getDisplayName());
            errorApiResponse.setStatus((long) response.getStatus());
            errorApiResponse.setActive(new Boolean(networkedClusters.get(port)));
        } else if ((portStatus == null && (deactivate != null && deactivate.equalsIgnoreCase("deactivate"))) || (portStatus != null && (deactivate != null && deactivate.equalsIgnoreCase("deactivate")))) {
            networkedClusters.put(port, StatusCode.FALSE.getDisplayName());
            errorApiResponse.setStatus((long) response.getStatus());
            errorApiResponse.setActive(new Boolean(networkedClusters.get(port)));
        } else if((portStatus == null && (unresponsive != null && unresponsive.equalsIgnoreCase("unresponsive"))) || (portStatus != null && (unresponsive != null && unresponsive.equalsIgnoreCase("unresponsive")))) {
            networkedClusters.put(port, StatusCode.UNRESPONSIVE.getDisplayName());
            errorApiResponse.setStatus((long)500);
            throw new IllegalArgumentException("Something went wrong");
        } else if(portStatus == null){
            networkedClusters.put(port, StatusCode.TRUE.getDisplayName());
            errorApiResponse.setStatus((long) response.getStatus());
            errorApiResponse.setActive(new Boolean(networkedClusters.get(port)));
        } else if(portStatus.equalsIgnoreCase("Unresponsive")){
            errorApiResponse.setStatus((long) 500);
            throw new IllegalArgumentException("Something went wrong");
        } else{
            errorApiResponse.setStatus((long) response.getStatus());
            errorApiResponse.setActive(new Boolean(networkedClusters.get(port)));
        }

        return errorApiResponse;
    }

}
